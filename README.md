# Order API For Burger Queen

**Kelompok**: Bebek-Bebek Lucu Anonim (C-12)

**Anggota yang mengerjakan API ini** :   
	- Julia Ningrum 					(1706979322)  
	- Kevin Christian Chandra 			(1706039976) 
 
**Deskripsi Aplikasi**  
Burger Queen adalah website yang menawarkan kustomisasi burger sesuai dengan minat customer. Burger Queen sendiri adalah tempat makan fast food baru buatan kami, Bebek-Bebek Lucu Anonim. Burger yang ditawarkan memiliki berbagai macam jenis roti, topping dan ukuran yang bisa dipilih oleh pelanggan.  

**Link Heroku**  
[burgerqueen-order.herokuapp.com](http://burgerqueen-order.herokuapp.com)