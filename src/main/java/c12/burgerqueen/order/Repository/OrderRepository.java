package c12.burgerqueen.order.Repository;

import c12.burgerqueen.order.Order.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    Order findById(long id_order);
}
