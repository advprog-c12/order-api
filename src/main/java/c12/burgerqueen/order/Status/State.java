package c12.burgerqueen.order.Status;

import c12.burgerqueen.order.Order.Order;

public interface State {
	
	public String toString();

	public void nextState(Order order);

	public boolean isCancelable();
}