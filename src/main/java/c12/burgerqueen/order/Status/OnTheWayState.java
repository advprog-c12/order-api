package c12.burgerqueen.order.Status;

import c12.burgerqueen.order.Order.Order;

public class OnTheWayState implements State {
    @Override
    public String toString() {
        return "On The Way";
    }

    @Override
    public void nextState(Order order) {
        order.setStatus(new DeliveredState());
    }

    @Override
    public boolean isCancelable() {
        return false;
    }
}