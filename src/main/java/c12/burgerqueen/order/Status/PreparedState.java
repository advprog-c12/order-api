package c12.burgerqueen.order.Status;

import c12.burgerqueen.order.Order.Order;

public class PreparedState implements State {
    @Override
    public String toString() {
        return "Prepared";
    }

    @Override
    public void nextState(Order order) {
	    order.setStatus(new OnTheWayState());
    }

    @Override
    public boolean isCancelable() {
        return true;
    }
}