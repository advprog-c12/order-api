package c12.burgerqueen.order.Status;

import c12.burgerqueen.order.Order.Order;

public class OrderPlacedState implements State {
    @Override
    public String toString() {
        return "Order Placed";
    }

    @Override
    public void nextState(Order order) {
        order.setStatus(new PreparedState());
    }

    @Override
    public boolean isCancelable() {
        return true;
    }
}