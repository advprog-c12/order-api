package c12.burgerqueen.order.Status;

import c12.burgerqueen.order.Order.Order;

public class DeliveredState implements State {
    @Override
	public String toString() {
		return "Delivered";
	}

    @Override
    public void nextState(Order order) {
    }

	@Override
	public boolean isCancelable() {
		return false;
	}
}