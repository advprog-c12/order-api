package c12.burgerqueen.order.Services;

import c12.burgerqueen.order.Order.Order;
import c12.burgerqueen.order.Repository.OrderRepository;
import c12.burgerqueen.order.Status.CancelledState;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Service
public class OrderService {

    @Autowired
    private OrderRepository repository;

    private TaskScheduler scheduler;

    private Runnable createRunnable(Order order){
        Runnable aRunnable = new Runnable(){
            public void run(){
                updateNextStatus(order);
            }
        };
        return aRunnable;
    }

    public void updateNextStatus(Order order) {
        order.nextStatus();
        repository.save(order);
    }

    @Async
    public void updateOrderStatus(Order order, int milis) {
        ScheduledExecutorService localExecutor = Executors.newSingleThreadScheduledExecutor();
        scheduler = new ConcurrentTaskScheduler(localExecutor);
        scheduler.schedule(createRunnable(order), new Date(order.getTimestamp().getTime() + milis));
        scheduler.schedule(createRunnable(order), new Date(order.getTimestamp().getTime() + (milis*2)));
        scheduler.schedule(createRunnable(order), new Date(order.getTimestamp().getTime() + (milis*3)));
    }

    public String findAll() {
        List<Order> orders = (List<Order>) repository.findAll();
        String json_orders = new Gson().toJson(orders);
        return json_orders;
    }

    public String getOrder(long id_order) {
        Order order = repository.findById(id_order);
        String order_json = new Gson().toJson(order);
        return order_json;
    }

    public String addOrder(Order order) {
        repository.save(order);
        this.updateOrderStatus(order, 10000);
        String order_json = new Gson().toJson(order);
        return order_json;
    }

    public String getOrderStatus(long id_order) {
        Order order = repository.findById(id_order);
        String gson = new Gson().toJson(order);
        String status = new Gson().fromJson(gson, JsonObject.class).get("status").getAsString();
        return status;
    }

    public String cancelOrder(long id_order) {
        Order order = repository.findById(id_order);
        order.setState(new CancelledState());
        repository.save(order);
        return this.getOrder(id_order);
    }
}
