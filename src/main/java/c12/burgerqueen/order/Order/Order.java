package c12.burgerqueen.order.Order;

import c12.burgerqueen.order.Status.CancelledState;
import c12.burgerqueen.order.Status.OrderPlacedState;
import c12.burgerqueen.order.Status.State;

import javax.persistence.*;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.ArrayList;

@Entity
@Table(name = "Orders")
public class Order {

   @Id
   @GeneratedValue
   @Column(name = "id_order")
   private long id_order;

   @Column(name = "id_akun")
   private long id_akun;

   @Transient
   private State state;

   @Column(name = "status")
   private String status;

   @Column(name = "cancelable")
   private boolean cancelable;

   @Column(name = "timestamp_order")
   private Timestamp timestamp;

   private ArrayList<Burger> burgers;

   public Order() {
   }

   public Order(long id_akun){
      this.id_akun = id_akun;
      this.timestamp = new Timestamp(System.currentTimeMillis());
      this.state = new OrderPlacedState();
      this.status = "Order Placed";
      this.cancelable = this.state.isCancelable();
   }

   public Order(long id_akun, ArrayList<Burger> burgers){
      this.id_akun = id_akun;
      this.timestamp = new Timestamp(System.currentTimeMillis());
      this.state = new OrderPlacedState();
      this.status = this.getStatus();
      this.cancelable = this.state.isCancelable();
      this.burgers = burgers;
   }

   public void setStatus(State state){
      this.state = state;
      this.status = state.toString();
   }

   public void setTimestamp(Timestamp timestamp) {
      this.timestamp = timestamp;
   }

   public void setId_akun(long id_akun) {
      this.id_akun = id_akun;
   }

   public void setId_order(long id_order) {
      this.id_order = id_order;
   }

   public void setState(State state) {
      this.state = state;
      this.setStatus(this.state.toString());
      this.cancelable = this.state.isCancelable();
   }

   public void setStatus(String status) {
      this.status = status;
   }

   public String getStatus(){
      return this.state.toString();
   }

   public void nextStatus() {
       this.state.nextState(this);
       this.status = this.state.toString();
       this.cancelable = this.state.isCancelable();
   }

   public void cancelOrder() {
       this.setState(new CancelledState());
   }

   public long getId_order() {
      return this.id_order;
   }

   public long getId_akun() {
      return this.id_akun;
   }

   public Timestamp getTimestamp() {
      return this.timestamp;
   }

   public boolean getCancelable() {
      return this.state.isCancelable();
   }

   public State getState() { return this.state; }

   public ArrayList<Burger> getBurgers() {
      return burgers;
   }
}