package c12.burgerqueen.order.Order;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;

public class Burger {

    @Id
    @Column(name = "id_burger")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "cost")
    private int cost;

    @Column(name = "amount")
    private int amount;

    @ManyToMany
    private ArrayList<Order> orders;

    public Burger(String id, String name, String description, int cost, int amount, Order order) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.amount = amount;
        this.orders.add(order);
    }
}
