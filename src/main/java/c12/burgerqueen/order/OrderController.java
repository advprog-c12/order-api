package c12.burgerqueen.order;

import c12.burgerqueen.order.Order.Order;
import c12.burgerqueen.order.Services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/getOrder/{id_akun}/{id_order}")
    public String getOrder(@PathVariable long id_akun, @PathVariable long id_order) {
        String order_json = orderService.getOrder(id_order);
        return order_json;
    }

    @GetMapping("/addOrder/{id_akun}")
    public String addOrder(@PathVariable long id_akun) {
        Order order = new Order(id_akun);
        return orderService.addOrder(order);
    }

    @GetMapping("/getAllOrder")
    public String getAllOrder() {
        String orders_json = orderService.findAll();
        return orders_json;
    }

    @CrossOrigin(origins = "*")
    @PostMapping(
            value = "/order/make-order-from-json",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}
    )
    public @ResponseBody Order postOrderFromJSON(@RequestBody Order order) {
        System.out.println("==== in greeting ====");
        System.out.println(order);
        return new Order(10000, order.getBurgers());
    }
}
