package c12.burgerqueen.order;

import c12.burgerqueen.order.Order.Order;
import c12.burgerqueen.order.Services.OrderService;
import c12.burgerqueen.order.Status.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatusController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/getOrderStatus/{id_akun}/{id_order}")
    public String getOrderStatus(@PathVariable long id_akun, @PathVariable long id_order) {
        String status = orderService.getOrderStatus(id_order);
        return status;
    }

    @GetMapping("/cancelOrder/{id_akun}/{id_order}")
    public String cancelOrder(@PathVariable long id_akun, @PathVariable long id_order) {
        return orderService.cancelOrder(id_order); }
}
