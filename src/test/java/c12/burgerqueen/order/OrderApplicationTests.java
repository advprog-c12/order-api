package c12.burgerqueen.order;

import c12.burgerqueen.order.Order.Order;
import c12.burgerqueen.order.Status.CancelledState;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class OrderApplicationTests {

    @Autowired
    private MockMvc mockMvc;

	@Test
	public void testOrderSetterGetter() throws Exception {
		Order order = new Order(1);
		order.setId_akun(2);
		assertEquals(2, order.getId_akun());

		order.setId_order(2);
		assertEquals(2, order.getId_order());

		order.setTimestamp(Timestamp.valueOf("2019-5-12 13:50:21"));
		assertEquals("2019-05-12 13:50:21.0", order.getTimestamp().toString());

		order.setState(new CancelledState());
		assertEquals("Cancelled", order.getStatus());
		assertEquals("Cancelled", order.getState().toString());
	}

	@Test
	public void addAndGetOrder() throws Exception {
		mockMvc.perform(get("/addOrder/1"))
			.andExpect(content().string(containsString("\"id_akun\":1")));
		mockMvc.perform(get("/getOrder/1/1"))
			.andExpect(content().string(containsString("\"id_akun\":1")));
	}

	@Test
	public void getAllOrder() throws Exception {
		mockMvc.perform(get("/addOrder/1"))
			.andExpect(content().string(containsString("\"id_akun\":1")));
		mockMvc.perform(get("/getAllOrder"))
			.andExpect(content().string(containsString("id_akun")));
	}
}
