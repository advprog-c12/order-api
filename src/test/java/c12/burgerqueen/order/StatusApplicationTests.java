package c12.burgerqueen.order;

import c12.burgerqueen.order.Order.Order;
import c12.burgerqueen.order.Status.CancelledState;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StatusApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testAllStatusStateAndCancelable() throws Exception {
        Order order = new Order(1);
        assertEquals("Order Placed", order.getStatus());
        assertTrue(order.getCancelable());

        order.nextStatus();
        assertEquals("Prepared", order.getStatus());
        assertTrue(order.getCancelable());

        order.nextStatus();
        assertEquals("On The Way", order.getStatus());
        assertFalse(order.getCancelable());

        order.nextStatus();
        assertEquals("Delivered", order.getStatus());
        assertFalse(order.getCancelable());
        order.nextStatus();
        assertEquals("Delivered", order.getStatus());

        order = new Order(1);
        order.cancelOrder();
        assertEquals("Cancelled", order.getStatus());
        order.nextStatus();
        assertEquals("Cancelled", order.getStatus());
        assertFalse(order.getCancelable());
    }

    @Test
    public void getOrderStatus() throws Exception {
        mockMvc.perform(get("/addOrder/1"))
            .andExpect(content().string(containsString("\"id_akun\":1")));
        mockMvc.perform(get("/addOrder/1"))
            .andExpect(content().string(containsString("\"id_akun\":1")));
        mockMvc.perform(get("/getOrderStatus/1/5"))
            .andExpect(content().string(containsString("Order Placed")));
    }

    @Test
    public void cancelOrder() throws Exception {
        mockMvc.perform(get("/addOrder/1"))
            .andExpect(content().string(containsString("\"id_akun\":1")));
        mockMvc.perform(get("/cancelOrder/1/1"))
            .andExpect(content().string(containsString("Cancelled")));
    }
}
